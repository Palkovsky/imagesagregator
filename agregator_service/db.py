import redis
redis_server = redis.Redis("localhost")

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine

Base = declarative_base()
engine = create_engine('sqlite:///development.db', echo=False)
#engine = create_engine('mysql://root:root@localhost/imgs_agregator?charset=utf8', pool_recycle=3306, echo=False)


from sqlalchemy.orm import sessionmaker, scoped_session
SessionFactory = sessionmaker(bind=engine)

def create_session():
	return SessionFactory()

def create_scoped_session():
	return scoped_session(SessionFactory)

session = create_session()

def default_redis():
	print("Settig Redis fields, to defaults...")

	if redis_server.get("initial_parse_interval") == None:
		redis_server.set("initial_parse_interval", 60)
	if redis_server.get("parse_interval") == None:
		redis_server.set("parse_interval", 15)

	redis_server.set("parsing", False)
	redis_server.set("initial_parsing", False)
	redis_server.set("parsing_provider", None)
	redis_server.set("initial_parsing_provider", None)
