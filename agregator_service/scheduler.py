from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.jobstores.base import JobLookupError
from apscheduler.schedulers import SchedulerNotRunningError
from db import redis_server

INITIAL_SCHEDULER_ID = "initial_parse_id"
PARSE_SCHEDULER_ID = "parse_id"

scheduler = BackgroundScheduler()

from parse import execute

def schedule_initial_task(minutes = None):
	if minutes == None:
		minutes = int(redis_server.get("initial_parse_interval"))
	scheduler.add_job(execute, 'interval', args=[True], minutes=minutes, id=INITIAL_SCHEDULER_ID) #inital parser

def schedule_parser_task(minutes = None):
	if minutes == None:
		minutes = int(redis_server.get("parse_interval"))
	scheduler.add_job(execute, 'interval', args=[False], minutes=minutes, id=PARSE_SCHEDULER_ID) #parser task

def schedule_tasks():
	try:
		global scheduler
		scheduler.shutdown(wait=False)
		scheduler = BackgroundScheduler()
	except SchedulerNotRunningError:
		pass
	schedule_initial_task()
	schedule_parser_task()
	scheduler.start()
	scheduler.print_jobs()
