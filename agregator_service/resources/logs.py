import falcon
from db import create_scoped_session
from utils import Response, FullValidator, meta, error, pagination, paginate, isint
from models import Log

MAX_PER_PAGE = 30
FILTERS = ["all", "normal", "error"]

class Collection(object):

	def on_get(self, req, resp):
		session = create_scoped_session()

		page = 1
		try:
			page = int(req.context["params"].get("page"))
		except:
			pass

		per_page = MAX_PER_PAGE
		try:
			per_page = int(req.context["params"].get("per"))
			if per_page > MAX_PER_PAGE:
				per_page = MAX_PER_PAGE
			elif per_page < 1:
				per_page = 1
		except:
			pass

		if isinstance(req.context["params"].get("providers"), str):
			if req.context["params"]["providers"] == "all":
				req.context["params"]["providers"] = [provider.id for provider in session.query(Provider).all()]
			else:
				req.context["params"]["providers"] = [req.context["params"].get("providers")]
		if req.context["params"].get("providers") == None:
			req.context["params"]["providers"] = []

		filter = req.context["params"].get("type") if req.context["params"].get("type") in FILTERS else "all"

		provider_ids = [int(provider_id) for provider_id in req.context["params"].get("providers") if isint(provider_id)]

		query = session.query(Log)
		if len(provider_ids) > 0:
			query = query.filter(Log.provider_id.in_(provider_ids))

		if filter == "normal":
			query = query.filter_by(error = False)
		elif filter == "error":
			query = query.filter_by(error = True)

		query = query.order_by(Log.id.desc())
		
		paginated = paginate(query, page = page, per_page = per_page)
		query =  paginated[0]
		total_pages = paginated[1]

		logs = [media.as_dict() for media in query]

		session.remove()

		resp.status = falcon.HTTP_200
		resp.body = Response.build(meta = meta(), data = logs, pagination = pagination(total=total_pages, current=page))