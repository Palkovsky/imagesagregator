import falcon, math, random
from os import environ
from db import create_scoped_session, engine
from models import Media, Provider
from utils import Response, FullValidator, meta, error, pagination, paginate, isint, build_medias_query

MAX_PER_PAGE = 50
MAX_NEWEST_PER_PAGE = 5
FILTERS = ["all", "image", "video", "gallery", "youtube"]

def before_action():
	pass

def execute(q):
	medias = []
	with engine.connect() as con:
		rs = con.execute(q[0])
		for r in rs:
			created_at = r[4] if isinstance(r[4], str) or r[4] == None else r[4].strftime('%d-%m-%Y %H:%M')
			added_at = r[5] if isinstance(r[5], str) or r[5] == None else r[5].strftime('%d-%m-%Y %H:%M')
			media = {"id" : r[0], "base_url" : r[1], "caption" : r[2], "description" : r[3], "created_at" : created_at,
			"added_at" : added_at, "media_type" : r[6], "provider_id" : r[9]}

			if media["media_type"] == "gallery":
				entries = []

				gq = 'SELECT * FROM medias WHERE gallery_id = ' + str(r[0])
				grs = con.execute(gq)
				for gr in grs:
					entries.append({"base_url" : gr[1], "caption" : gr[2], "description" : gr[3], "media_type" : gr[6], "data" : {"source_url" : gr[7]}})

				media["data"] = entries 
			else:
				media["data"] = {"source_url" : r[7]}

			medias.append(media)

		count = con.execute(q[1]).fetchone()[0]
	return (medias, count)

def fetch(req, newest = False):
	page = 1
	try:
		page = int(req.context["params"].get("page"))
	except:
		pass

	per_page = 50
	try:
		per_page = int(req.context["params"].get("per"))
		if per_page > MAX_PER_PAGE:
			per_page = MAX_PER_PAGE
		elif per_page < 1:
			per_page = 1
	except:
		pass
	
	if isinstance(req.context["params"].get("type"), str):
		req.context["params"]["type"] = [req.context["params"].get("type")]
	if req.context["params"].get("type") == None:
		req.context["params"]["type"] = []

	if isinstance(req.context["params"].get("providers"), str):
		if req.context["params"]["providers"] == "all":
			req.context["params"]["providers"] = [provider.id for provider in session.query(Provider)]
		else:
			req.context["params"]["providers"] = [req.context["params"].get("providers")]
	if req.context["params"].get("providers") == None:
		req.context["params"]["providers"] = []	

	filters = [filter for filter in req.context["params"].get("type") if filter in FILTERS]
	provider_ids = [int(provider_id) for provider_id in req.context["params"].get("providers") if isint(provider_id)]
	if "all" in filters:
		filters = FILTERS

	if newest:
		medias = []
		count = 0

		session = create_scoped_session()
		if len(provider_ids) == 0:
			provider_ids = [provider.id for provider in session.query(Provider)]
		session.remove()

		newest_per_page = per_page * len(provider_ids)
		if newest_per_page > MAX_NEWEST_PER_PAGE:
			newest_per_page = MAX_NEWEST_PER_PAGE

		for provider_id in provider_ids:
			q = build_medias_query(page, newest_per_page, filters, [provider_id])

			ex = execute(q)
			medias = medias + ex[0]
			count += ex[1]

		random.shuffle(medias)
	else:
		q = build_medias_query(page, per_page, filters, provider_ids)
		ex = execute(q)
		medias = ex[0]
		count = ex[1]

	total_pages = math.ceil(float(count)/float(per_page))
	if total_pages == 0:
		total_pages = 1

	return {"data" : medias, "params" : {"page" : page, "per_page" : per_page, "total_pages" : total_pages, "filters" : filters}}

class Collection(object):

	def on_get(self, req, resp):
		before_action()

		fetched = fetch(req)
		medias = fetched["data"]
		params = fetched["params"]

		resp.status = falcon.HTTP_200
		resp.body = Response.build(meta = meta(), data = medias, pagination = pagination(total=params["total_pages"], current=params["page"]))

class CollectionNewest(object):

	def on_get(self, req, resp):
		before_action()

		fetched = fetch(req, newest = True)
		medias = fetched["data"]
		params = fetched["params"]

		resp.status = falcon.HTTP_200
		resp.body = Response.build(meta = meta(), data = medias, pagination = pagination(total=params["total_pages"], current=params["page"]))

class Item(object):

	def on_delete(self, req, resp, **kwargs):
		before_action()

		session = create_scoped_session()

		media = session.query(Media).filter_by(id=kwargs.get("media_id"))

		if media == None:
			resp.status = falcon.HTTP_404
			resp.body = Response.build(meta = meta(False), errors = error("not found"))
		else:
			media.delete()
			session.commit()
			resp.status = falcon.HTTP_204

		session.remove()

	def on_put(self, req, resp, **kwargs):
		before_action()
		session = create_scoped_session()

		media = session.query(Media).filter_by(id=kwargs.get("media_id"))

		if media == None:
			resp.status = falcon.HTTP_404
			resp.body = Response.build(meta = meta(False), errors = error("not found"))
		else:
			
			params = req.context['doc']

			#only caption and description editable
			rules = {
				"caption" : {
					"type" : "string"
				},
				"description" : {
					"type" : "string"
				}
			}

			validator = FullValidator(rules)
			validator.validate(params)

			if validator.fails():
				resp.status = falcon.HTTP_400
				resp.body = Response.build(meta = meta(False), errors = validator.errors())
			else:
				media.update({"caption" : params.get("caption", None), "description" : params.get("description", None)})
				session.commit()

				resp.status = falcon.HTTP_200
				resp.body = Response.build(meta = meta(), data = media.first().as_dict())

			session.remove()