import falcon, os
from utils import Response, FullValidator, meta, error, toint
from scheduler import scheduler, schedule_tasks, INITIAL_SCHEDULER_ID, PARSE_SCHEDULER_ID
from db import redis_server

class ParserInfo(object):

	def on_get(self, req, resp):

		parsing = True if redis_server.get("parsing").decode('utf-8') == "True" else False
		initial_parsing = True if redis_server.get("initial_parsing").decode('utf-8') == "True" else False
		parsing_provider = int(redis_server.get("parsing_provider")) if redis_server.get("parsing_provider").decode('utf-8') != "None" else None
		initial_parsing_provider = int(redis_server.get("initial_parsing_provider")) if redis_server.get("initial_parsing_provider").decode('utf-8') != "None" else None
		last_parse = redis_server.get("last_parse").decode('utf-8') if redis_server.get("last_parse") != None else None
		last_successful_parse = redis_server.get("last_successful_parse").decode('utf-8') if redis_server.get("last_successful_parse") != None else None

		data = {
			"parsing" : parsing,
			"initial_parsing" : initial_parsing,
			"parsing_provider" : parsing_provider,
			"initial_parsing_provider" : initial_parsing_provider,
			"last_parse" : last_parse,
			"last_successful_parse" : last_successful_parse
		}
		resp.status = falcon.HTTP_200
		resp.body = Response.build(meta = meta(), data = data)


class ParserConfig(object):

	def on_get(self, req, resp):
		parse_interval = int(redis_server.get("parse_interval"))
		initial_parse_interval = int(redis_server.get("initial_parse_interval"))

		data = {
			"parse_interval" : parse_interval,
			"initial_parse_interval" : initial_parse_interval
		}

		resp.status = falcon.HTTP_200
		resp.body = Response.build(meta=meta(), data=data)

	def on_post(self, req, resp):

		params = req.context["doc"]

		rules = {
			"parse_interval" : {
				"min" : 1,
				"coerce" : toint
			},
			"initial_parse_interval" : {
				"min" : 1,
				"coerce" : toint
			}
		}

		validator = FullValidator(rules)
		validator.validate(params)

		if validator.fails():
			resp.status = falcon.HTTP_400
			resp.body = Response.build(meta = meta(False), errors = validator.errors())
		else:

			parse_interval = params.get("parse_interval")
			initial_parse_interval = params.get("initial_parse_interval")

			change = False

			if parse_interval != None:
				redis_server.set("parse_interval", parse_interval)
				change = True
			if initial_parse_interval != None:
				redis_server.set("initial_parse_interval", initial_parse_interval)
				change = True

			if change:
				schedule_tasks()

			parse_interval = int(redis_server.get("parse_interval"))
			initial_parse_interval = int(redis_server.get("initial_parse_interval"))

			data = {
				"parse_interval" : parse_interval,
				"initial_parse_interval" : initial_parse_interval
			}

			resp.status = falcon.HTTP_200
			resp.body = Response.build(meta=meta(), data=data)