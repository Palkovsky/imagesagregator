import falcon
from scheduler import scheduler, schedule_initial_task, schedule_parser_task, INITIAL_SCHEDULER_ID, PARSE_SCHEDULER_ID
from utils import Response, meta, error
from apscheduler.jobstores.base import JobLookupError

def handle_request(req, resp, job_id):
		action = req.context["params"].get("action")
		if action != None:
			resp.status = falcon.HTTP_405
			resp.body = Response.build(meta = meta(False), errors = error("can't recognize action"))
		else:
			resp.status = falcon.HTTP_405
			resp.body = Response.build(meta = meta(False), errors = error("undefined action"))	

class InitialParserController(object):
	
	def on_post(self, req, resp):
		handle_request(req, resp, INITIAL_SCHEDULER_ID)

class ParserController(object):
	
	def on_post(self, req, resp):
		handle_request(req, resp, PARSE_SCHEDULER_ID)