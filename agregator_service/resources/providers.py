import falcon
from db import create_scoped_session
from utils import Response, FullValidator, meta, error
from models import Provider

def before_action():
	pass

class Collection(object):

	def on_post(self, req, resp):
		before_action()

		session = create_scoped_session()
		params = req.context["doc"]

		rules = {
			"url" : {
				"required" : True,
				"type" : "string",
				"unique" : [session, Provider.url]
			},
			"name" : {
				"required" : True,
				"type" : "string",
				"unique" : [session, Provider.name]
			},
			"description" : {
				"type" : "string"
			},
			"thumbnail_url" : {
				"type" : "string"
			}
		}

		validator = FullValidator(rules)
		validator.validate(params)

		if validator.fails():
			resp.status = falcon.HTTP_400
			resp.body = Response.build(meta = meta(False), errors = validator.errors())
		else:
			newProvider = Provider(url = params["url"], name = params["name"], description = params.get("description", None), thumbnail_url = params.get("thumbnail_url", None))
			session.add(newProvider)
			session.commit()

			resp.status = falcon.HTTP_201
			resp.body = Response.build(meta = meta(), data = newProvider.as_dict())

		session.remove()


	def on_get(self, req, resp):
		before_action()
		session = create_scoped_session()
		empty = True if req.context["params"].get("initially_parsed", None) == None else False
		initially_parsed = True if req.context["params"].get("initially_parsed") == "true" else False

		if empty:
			providers = [provider.as_dict() for provider in session.query(Provider)]
		else:
			providers = [provider.as_dict() for provider in session.query(Provider).filter_by(initially_parsed=initially_parsed)]
			
		resp.status = falcon.HTTP_200
		resp.body = Response.build(meta = meta(), data = providers)
		session.remove()

class Item(object):

	def on_get(self, req, resp, **kwargs):
		before_action()

		session = create_scoped_session()
		provider = session.query(Provider).get(kwargs.get("provider_id"))

		if provider == None:
			resp.status = falcon.HTTP_404
			resp.body = Response.build(meta = meta(False), errors = error("not found"))
		else:
			resp.status = falcon.HTTP_200
			resp.body = Response.build(meta = meta(), data = provider.as_dict())

		session.remove()


	def on_delete(self, req, resp, **kwargs):
		before_action()

		session = create_scoped_session()
		provider = session.query(Provider).filter_by(id=kwargs.get("provider_id"))

		if provider == None:
			resp.status = falcon.HTTP_404
			resp.body = Response.build(meta = meta(False), errors = error("not found"))
		else:
			session.delete(provider.first())
			session.commit()
			resp.status = falcon.HTTP_204
		session.remove()


	def on_put(self, req, resp, **kwargs):
		before_action()

		session = create_scoped_session()
		provider = session.query(Provider).filter_by(id=kwargs.get("provider_id"))

		if provider == None:
			resp.status = falcon.HTTP_404
			resp.body = Response.build(meta = meta(False), errors = error("not found"))
		else:

			params = req.context['doc']

			rules = {
				"url" : {
					"required" : True,
					"type" : "string",
					"unique" : [session, Provider.url, Provider.id, provider.first().id]
				},
				"name" : {
					"required" : True,
					"type" : "string",
					"unique" : [session, Provider.name, Provider.id, provider.first().id]
				},
				"description" : {
					"type" : "string"
				},
				"thumbnail_url" : {
					"type" : "string"
				}
			}

			validator = FullValidator(rules)
			validator.validate(params)

			if validator.fails():
				resp.status = falcon.HTTP_400
				resp.body = Response.build(meta = meta(False), errors = validator.errors())
			else:				
				provider.update({"url" : params["url"], "name" : params["name"], "description" : params.get("description", None), "thumbnail_url" : params.get("thumbnail_url", None)})
				session.commit()

				resp.status = falcon.HTTP_200
				resp.body = Response.build(meta = meta(), data = provider.first().as_dict())
			session.remove()