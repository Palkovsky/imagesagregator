"""Add medias table

Revision ID: eccc23710a94
Revises: 5c7d528f5c51
Create Date: 2016-06-20 14:29:25.455776

"""

# revision identifiers, used by Alembic.
revision = 'eccc23710a94'
down_revision = '5c7d528f5c51'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
    		'medias',
    		sa.Column(
    			'id', sa.Integer, primary_key=True
    		),
    		sa.Column(
    			'base_url', sa.String(255), nullable=False, unique=True
    		),
            sa.Column(
                'caption', sa.String(255)
            ),
            sa.Column(
                'description', sa.Text
            ),
    		sa.Column(
    			'created_at', sa.DateTime
    		),
    		sa.Column(
    			'added_at', sa.DateTime
    		),
    		sa.Column(
    			'type', sa.String(50), nullable=False
    		),
    		sa.Column(
    			'source_url', sa.String(255), unique=True, nullable=True
    		),
            sa.Column(
                'gallery_id', sa.Integer, index = True
            ),
            sa.Column(
                'provider_id', sa.Integer, index = True
            )
    	)

def downgrade():
    op.drop_table(
    	'medias'
    )
