"""add providers table

Revision ID: 5c7d528f5c51
Revises: 
Create Date: 2016-06-19 15:29:17.816753

"""

# revision identifiers, used by Alembic.
revision = '5c7d528f5c51'
down_revision = None
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
    		'providers',
    		sa.Column(
    			'id', sa.Integer, primary_key=True
    		),
    		sa.Column(
    			'url', sa.String(255), nullable=False, unique=True
    		),
    		sa.Column(
    			'name', sa.String(255), nullable=False, unique=True
    		),
    		sa.Column(
    			'description', sa.Text
    		),
            sa.Column(
                'last_parse', sa.DateTime
            ),
            sa.Column(
                'initially_parsed', sa.Boolean, default=False
            )
    	)


def downgrade():
    op.drop_table(
    	'providers'
    )
