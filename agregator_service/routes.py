from resources import providers
from resources import medias
from resources import logs
from resources import stats
from resources import parser_controllers

def define_routes(api):
	
	providers_collection = providers.Collection()
	provider_item = providers.Item()

	api.add_route('/providers', providers_collection)
	api.add_route('/providers/{provider_id}', provider_item)

	medias_collection = medias.Collection()
	api.add_route('/medias', medias_collection)
	newest_medias_collection = medias.CollectionNewest()
	api.add_route('/medias/newest', newest_medias_collection)
	medias_item = medias.Item()
	api.add_route('/medias/{media_id}', medias_item)

	logs_collection = logs.Collection()
	api.add_route('/logs', logs_collection)

	parser_info = stats.ParserInfo()
	api.add_route('/stats', parser_info)

	parser_config = stats.ParserConfig()
	api.add_route('/config', parser_config)

	initial_parser_controller = parser_controllers.InitialParserController()
	api.add_route('/initial_parser', initial_parser_controller)

	parser_controller = parser_controllers.ParserController()
	api.add_route('/parser', parser_controller)