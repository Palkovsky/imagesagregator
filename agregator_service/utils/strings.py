def find_between(s, first, last):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

def isint(value):
	try:
		int(value)
		return True
	except:
		return False

def toint(value):
    try:
        return int(value)
    except:
        return None

#IN: http://www.youtube.com/embed/uQTxsfKRjZU?rel=0&autohide=0&modestbranding=1&showinfo=0&fs=1&egm=1&wmode=transparent&iv_load_policy=3
#OUT: http://www.youtube.com/watch?v=uQTxsfKRjZU
def embeded_yt_to_url(embeded_url):
    if "watch?v=" in embeded_url:
        return embeded_url
    else:
        uid = embeded_url.split("/")[-1].split("?")[0].split("&")[0]
    return "http://www.youtube.com/watch?v=" + uid