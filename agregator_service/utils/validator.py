from restrainer import Validator, Constraint
from sqlalchemy.sql import exists
from sqlalchemy import and_, or_

class FullValidator(Validator):

	def __init__(self, rules, constrainers = []):
		super(FullValidator, self).__init__(rules, constrainers = constrainers)
		self.load_constraint(UniqueConstraint())


'''
	Constraint value should be iterable containing
		session at index 0
		model attribue at index 1 | Ex. User.email

		optional:
			except model attribute index 2
			except model value index 3
'''
class UniqueConstraint(Constraint):

	def name(self):
		return "unique"

	def validate(self, value, constraint_value, field_name, doc):
		session = constraint_value[0]
		model_attribute = constraint_value[1]

		if(len(constraint_value)) > 2:
			model_except_attribute = constraint_value[2]
			except_model_value = constraint_value[3]

			return not session.query(exists().where(model_except_attribute != except_model_value).where(model_attribute == value)).scalar()
		else:
			return not session.query(exists().where(model_attribute == value)).scalar()