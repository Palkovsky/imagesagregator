import requests, time, urllib
from bs4 import BeautifulSoup


def is_url(url):

	try:
		request = urllib.request.Request(url)
		request.get_method = lambda: 'HEAD'
		urllib.request.urlopen(request)
		return True
	except:
		return False


default_headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36'}
MAX_RESTARTS = 5

def get_soup(url, headers = None, cookies = {}):
	if headers == None:
		headers = default_headers
	try:
		page = requests.get(url, headers = headers, cookies = cookies)
		RESTARTS_COUNT = 0
	except requests.exceptions.ConnectionError:

		print("CONECTION ERROR OCCURED")
		restarts_count = 0
		fetched = False

		while(restarts_count < MAX_RESTARTS and not fetched):
			time.sleep(10)
			try:
				page = requests.get(url, headers = headers, cookies = cookies)
				fetched = True
			except requests.exceptions.ConnectionError:
				restarts_count += 1

		print("RESTARTS_COUNT: " + str(restarts_count))
	except:
		return None

	return BeautifulSoup(page.content, 'html.parser')

def to_soup(string):
	return BeautifulSoup(string, 'html.parser')