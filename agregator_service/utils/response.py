class Response(object):

	def __init__(self, data = None, meta = None, pagination = None, errors = [], data_wrapper = "data"):
		if data != None:
			self.response = {data_wrapper : data}
		if len(errors) > 0:
			if isinstance(errors, dict):
				errors = [errors]
			self.response = {"errors" : errors}
		if meta != None:
			self.response["meta"] = meta
		if pagination != None:
			self.response["pagination"] = pagination

	def get(self):
		return self.response

	@staticmethod
	def build(data = None, meta = None, pagination = None, errors = [], data_wrapper = "data"):
		response = Response(data=data, meta=meta, pagination=pagination, errors=errors, data_wrapper=data_wrapper)
		return response.get()

	def __getitem__(self, key):
		return self.response[key]

	def __setitem__(self, key, value):
		self.response[key] = value
		

def pagination(total, current, **kwargs):
	pagination = {"last_page" : total, "current_page" : current}
	for key, value in kwargs.items():
		pagination[key] = value
	return pagination

def meta(successful = True, **kwargs):
	meta = {"success" : successful}
	for key, value in kwargs.items():
		meta[key] = value
	return meta

def error(message, **kwargs):
	error = {"message" : message, "error_type" : kwargs.get("error_type", "other")}
	for key, value in kwargs.items():
		error[key] = value
	return error