import math

def paginate(query, page = None, per_page = None):
	count = query.count()
	total_pages = 1
	if isinstance(per_page, int):
		query = query.limit(per_page)
		total_pages = math.ceil(float(count)/float(per_page))
	if isinstance(page, int):
		query = query.offset((page-1) * per_page)
	if total_pages == 0:
		total_pages = 1
	return (query, total_pages)

def build_medias_query(page, per_page, types, provider_ids):

	provider_id_statement = "("
	for index, provider_id in enumerate(provider_ids):
		if index == len(provider_ids) - 1:
			provider_id_statement += ("provider_id = " + str(provider_id))
		else:
			provider_id_statement += ("provider_id = " + str(provider_id) + " OR ")
	provider_id_statement += ")"

	types_statement = "("
	for index, type in enumerate(types):
		if index == len(types) - 1:
			types_statement += ("type = '" + type + "'")
		else:
			types_statement += ("type = '" + type + "' OR ")
	types_statement += ")"

	order_statement = "ORDER BY added_at DESC"
	pagination_statement = "LIMIT " + str(per_page) + " OFFSET " + str((page-1) * per_page)

	types_statement = "" if types_statement == "()" else types_statement + ' AND '
	provider_id_statement = "" if provider_id_statement == "()" else provider_id_statement + ' AND '
	no_gallery_item_statement = "gallery_id IS NULL"

	return ('SELECT * FROM medias WHERE ' + types_statement + provider_id_statement + no_gallery_item_statement + ' ' + order_statement + ' ' + pagination_statement + ';',
		'SELECT COUNT(*) FROM medias WHERE ' + types_statement + provider_id_statement + no_gallery_item_statement + ';')