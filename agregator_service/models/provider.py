from db import Base, engine
from sqlalchemy import Column, ForeignKey, Integer, String, Text, DateTime, Boolean
from sqlalchemy.orm import relationship


class Provider(Base):

	__tablename__ = "providers"

	id = Column(Integer, primary_key=True)
	url = Column(String(255), unique=True, nullable=False)
	name = Column(String(255), unique=True, nullable=False)
	thumbnail_url = Column(String(255))
	description = Column(Text)
	last_parse = Column(DateTime())
	initially_parsed = Column(Boolean(), default=False)

	entries = relationship("Media", back_populates="provider",
		primaryjoin='and_(Media.provider_id==Provider.id)', cascade="all, delete-orphan")

	logs = relationship("Log", back_populates="provider",
		primaryjoin='and_(Log.provider_id==Provider.id)', cascade="all, delete-orphan")

	def as_dict(self, **kwargs):

		with engine.connect() as con:
			rs = con.execute('SELECT COUNT(*) FROM medias WHERE provider_id=' + str(self.id) + ' AND gallery_id IS NULL;')
			count = rs.fetchone()[0]

		base = {"id" : self.id, "url" : self.url, "name" : self.name, "description" : self.description,
				"last_parse" : self.last_parse.strftime('%d-%m-%Y %H:%M') if self.last_parse != None else None, 
				"initially_parsed" : self.initially_parsed, "thumbnail_url" : self.thumbnail_url,
				"item_count" : count}

		for key, value in kwargs:
			base[key] = value

		return base