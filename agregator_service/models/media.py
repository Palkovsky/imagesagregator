from db import Base
from sqlalchemy import Column, ForeignKey, Integer, String, Text, DateTime
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql import func
from datetime import datetime


class Media(Base):

	__tablename__ = 'medias'

	id = Column(Integer, primary_key=True)
	
	base_url = Column(String(255), nullable = False, unique = True)
	created_at = Column(DateTime(timezone=True), default=datetime.now())

	caption = Column(String(255))
	description = Column(Text)

	added_at = Column(DateTime, index = True)
	
	gallery_id = Column(Integer, ForeignKey(id), index = True)
	gallery = relationship("Gallery", backref = "items", remote_side = id)

	provider_id = Column(Integer, ForeignKey("providers.id"), index = True)
	provider = relationship("Provider", back_populates="entries",
		primaryjoin='and_(Media.provider_id==Provider.id)')

	media_type = Column('type', String(50), nullable = False)
	__mapper_args__ = {'polymorphic_on': media_type}

	def as_dict(self):
		return {"id" : self.id, "base_url" : self.base_url, "caption" : self.caption, "description" : self.description,
		"added_at" : self.added_at.strftime('%d-%m-%Y %H:%M'), "provider_id" : self.provider_id, "media_type" : self.media_type,
		"data" : self.data()}


class Image(Media):
	__mapper_args__ = {'polymorphic_identity' : 'image'}

	@declared_attr
	def source_url(cls):
		return Media.__table__.c.get('source_url', Column(String(255), unique = True))

	def data(self):
		return {"source_url" : self.source_url}

class Video(Media):
	__mapper_args__ = {'polymorphic_identity' : 'video'}

	@declared_attr
	def source_url(cls):
		return Media.__table__.c.get('source_url', Column(String(255), unique = True))

	def data(self):
		return {"source_url" : self.source_url}

#you tube differs from video with that it stores only you tube url(and is hosted on youtube, ofc)
class YouTube(Media):
	__mapper_args__ = {'polymorphic_identity' : 'youtube'}

	@declared_attr
	def source_url(cls):
		return Media.__table__.c.get('source_url', Column(String(255), unique = True))	

class Gallery(Media):
	__mapper_args__ = {'polymorphic_identity' : 'gallery'}

	def data(self):
		return [item.as_dict() for item in self.items]