from db import Base
from sqlalchemy import Column, ForeignKey, Integer, Boolean, Text, DateTime
from sqlalchemy.orm import relationship
from datetime import datetime

class Log(Base):

	__tablename__ = 'logs'

	id = Column(Integer, primary_key=True)

	message = Column(Text)
	error = Column(Boolean, default = False)

	created_at = Column(DateTime, default = datetime.now())

	provider_id = Column(Integer, ForeignKey('providers.id'))
	provider = relationship("Provider", back_populates="logs",
		primaryjoin='and_(Log.provider_id==Provider.id)')


	def as_dict(self):
		return {"id" : self.id, "message" : self.message, "error" : self.error, "created_at" : self.created_at.strftime('%d-%m-%Y %H:%M'),
			"provider_id" : self.provider_id}