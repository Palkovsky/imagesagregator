#i want test to be done on real data, only assure that properly formatted records are returned
#just make sure there are no exceptions

import random

VIDEO_FORMATS = (".webm", ".mp4", ".WEBM", ".MP4")
IMAGES_FORMATS = (".jpg", ".jpeg", ".png", ".gif", ".bnp", ".JPG", ".JPEG", ".PNG", ".GIF", ".BNP")


def parser_test(parser, url, min_page, max_page, tries = 20, per = 10, pages = [], offset=2):

	if url.endswith("/"):
		url = url[:-1]

	if len(pages) == 0:
		for i in range(tries):
			pages.append(random.randint(min_page, max_page))

	for page in pages:
		entities = parser.parsed_page(url  + "/" + str(page))

		#allow offset, in case of some ezoteric hosting
		assert len(entities) >= per - offset and len(entities) <= per

		for entity in entities:
			assert entity["type"] == "image" or entity["type"] == "gallery" or entity["type"] == "video" or entity["type"] == "youtube"
			assert "data" in entity
			assert "type" in entity

			if entity["type"] != "gallery":
				assert "source_url" in entity["data"]

				source_url = entity["data"]["source_url"]

				if entity["type"] == "video":
					if '?' in source_url:
						source_url = source_url[:source_url.find('?')]
					assert source_url.endswith(VIDEO_FORMATS)
				elif entity["type"] == "image":
					if '?' in source_url:
						source_url = source_url[:source_url.find('?')]
					assert source_url.endswith(IMAGES_FORMATS)
				elif entity["type"] == "youtube":
					print(entity)
					print(source_url)
					assert "www.youtube.com/watch?v=" in source_url

			else:
				#http://demotywatory.pl/4656122/Piosenki-ktore-krolowaly-na-listach-przebojow-w-ciagu#obrazek-6 - this not passing gallery of videos
				assert "images" in entity["data"] or "items" in entity["data"]
				if "images" in entity["data"]:
					assert len(entity["data"]["images"]) > 0
				elif "items" in entity["data"]:
					assert len(entity["data"]["items"]) > 0