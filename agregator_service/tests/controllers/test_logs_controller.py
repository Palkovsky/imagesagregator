import falcon
from models import Log, Provider
from db import create_session
from resources.logs import MAX_PER_PAGE

session = create_session()

def test_simple_get(client):
	resp = client.get("/logs")
	assert resp.status == falcon.HTTP_OK
	assert resp.json["meta"]["success"] == True
	assert len(resp.json["data"]) <= MAX_PER_PAGE
	assert len(resp.json["data"]) == MAX_PER_PAGE or len(resp.json["data"]) == session.query(Log).count()

	#test ordering
	if len(resp.json["data"]) > 1:
		last_id = resp.json["data"][0]["id"]
		for i in range(1, len(resp.json["data"])):
			assert last_id > resp.json["data"][i]["id"]
			last_id = resp.json["data"][i]["id"]

def test_provider_specific_get(client):

	provider = Provider(name="sample", url="http://www.google.com")
	log = Log(message="sample", error=False, provider = provider)
	session.add(provider)
	session.add(log)
	session.commit()

	resp = client.get("/logs?providers=" + str(provider.id))
	assert resp.status == falcon.HTTP_OK
	assert resp.json["meta"]["success"] == True
	assert len(resp.json["data"]) == 1

	for log_item in resp.json["data"]:
		assert log_item["provider_id"] == provider.id

	session.query(Provider).filter_by(id=provider.id).delete()
	session.query(Log).filter_by(id=log.id).delete()
	session.commit()

def test_filter_specific_get(client):
	resp = client.get("/logs?type=error")
	assert resp.status == falcon.HTTP_OK

	for log_item in resp.json["data"]:
		assert log_item["error"] == True

	resp = client.get("/logs?type=normal")
	assert resp.status == falcon.HTTP_OK

	for log_item in resp.json["data"]:
		assert log_item["error"] == False