import falcon
from models import Provider
from db import create_session

session = create_session()

def test_simple_get(client):
	resp = client.get("/providers")
	assert resp.status == falcon.HTTP_OK
	assert resp.json["meta"]["success"] == True
	assert "pagination" not in resp.json
	assert len(resp.json["data"]) == session.query(Provider).count()

	for item in resp.json["data"]:
		provider = session.query(Provider).get(item["id"])

def test_get_only_initially_parsed(client):
	resp = client.get("/providers?initially_parsed=true")
	assert resp.status == falcon.HTTP_OK
	assert resp.json["meta"]["success"] == True
	assert "pagination" not in resp.json
	assert len(resp.json["data"]) == session.query(Provider).filter_by(initially_parsed = True).count()

	for item in resp.json["data"]:
		provider = session.query(Provider).get(item["id"])

		assert provider.initially_parsed == True

def test_get_only_not_initially_parsed(client):
	resp = client.get("/providers?initially_parsed=false")
	assert resp.status == falcon.HTTP_OK
	assert resp.json["meta"]["success"] == True
	assert "pagination" not in resp.json
	assert len(resp.json["data"]) == session.query(Provider).filter_by(initially_parsed = False).count()

	for item in resp.json["data"]:
		provider = session.query(Provider).get(item["id"])

		assert provider.initially_parsed == False