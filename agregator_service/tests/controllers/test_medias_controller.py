import falcon
from models import Media
from db import create_session
from resources.medias import MAX_PER_PAGE

session = create_session()

def test_simple_get(client):
	resp = client.get("/medias")
	assert resp.status == falcon.HTTP_OK
	assert resp.json["meta"]["success"] == True
	assert "pagination" in resp.json
	assert resp.json["pagination"]["current_page"] == 1
	assert len(resp.json["data"]) == MAX_PER_PAGE or len(resp.json["data"]) == session.query(Media).count()

def test_paginated_get(client):
	resp = client.get("/medias?per=1000")
	assert resp.status == falcon.HTTP_OK
	assert len(resp.json["data"]) <= MAX_PER_PAGE
