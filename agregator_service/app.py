from dotenv import load_dotenv
load_dotenv(".env")

import falcon
from routes import define_routes
from middleware import RequireJSON, JSONTranslator, QueryStringParser, CORSHeader, Authorize

application = falcon.API(
	middleware = [
			RequireJSON(),
			JSONTranslator(),
			QueryStringParser(),
			#Authorize(),
			CORSHeader()
		]
	)
define_routes(application)

from db import default_redis
default_redis()

from scheduler import schedule_tasks
schedule_tasks()