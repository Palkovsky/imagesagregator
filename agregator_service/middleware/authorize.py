import falcon, os

class Authorize(object):

	def process_request(self, req, resp):

		token = req.headers.get("AUTHORIZATION", "")

		if token != os.environ["AUTHORIZATION_TOKEN"]:
			raise falcon.HTTPError(falcon.HTTP_401, "Unauthorized", "Invalid authorization token.")