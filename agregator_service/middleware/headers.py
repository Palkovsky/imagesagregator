import falcon

class CORSHeader(object):
	
	def process_request(self, req, resp):
		resp.set_header('Access-Control-Allow-Origin', '*')
