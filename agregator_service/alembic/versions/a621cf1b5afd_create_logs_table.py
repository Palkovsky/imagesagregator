"""Create logs table

Revision ID: a621cf1b5afd
Revises: eccc23710a94
Create Date: 2016-07-01 12:21:48.788515

"""

# revision identifiers, used by Alembic.
revision = 'a621cf1b5afd'
down_revision = 'eccc23710a94'
branch_labels = None
depends_on = None

from alembic import op
from datetime import datetime
import sqlalchemy as sa


def upgrade():
    op.create_table(
    		'logs',
    		sa.Column(
    			'id', sa.Integer, primary_key=True
    		),
    		sa.Column(
    			'message', sa.Text
    		),
    		sa.Column(
    			'error', sa.Boolean, default = False
    		),
    		sa.Column(
    			'created_at', sa.DateTime, default = datetime.now()
    		),
            sa.Column(
                'provider_id', sa.Integer, index = True
            )
    	)


def downgrade():
    op.drop_table(
    	'logs'
    )
