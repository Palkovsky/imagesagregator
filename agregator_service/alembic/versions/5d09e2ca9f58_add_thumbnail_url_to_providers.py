"""add_thumbnail_url_to_providers

Revision ID: 5d09e2ca9f58
Revises: a621cf1b5afd
Create Date: 2016-07-05 13:08:54.149090

"""

# revision identifiers, used by Alembic.
revision = '5d09e2ca9f58'
down_revision = 'a621cf1b5afd'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('providers',
    	sa.Column('thumbnail_url', sa.String(255))
    )


def downgrade():
    op.drop_column('providers', 'thumbnail_url')
