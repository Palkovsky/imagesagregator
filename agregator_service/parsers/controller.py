 
'''
	This class takes care of inserting parsed data into
	db and makes sure there's no duplicates and stopping
	parsers when there's nothing more to parse.
'''

from time import time

from parsers import Parser
from models import Image, Gallery, Video, YouTube
from db import create_scoped_session

from sqlalchemy.exc import IntegrityError
from dateutil import parser

class ParseController(object):

	def __init__(self, parser, provider = None):

		self.DUPLICATE_FINISH_LIMIT = 60
		self.DUPLICATE_COUNT = 0

		self.PARSER = parser
		self.PROVIDER = provider

		self.parsed_items_count = 0
		self.duplicates_count = 0
		self.parse_time = 0
		self.parse_start_time = None
		self.exceptions = []

	def start_initial_parsing(self, start_page = None):
		self.parse_start_time = int(time())
		self.PARSER.run(initial = True, process = self.process_entities, start_page = start_page)

	def start_parsing(self):
		self.parse_start_time = int(time())
		self.PARSER.run(initial = False, process = self.process_entities)

	#method used for debugging
	def parse_pages(self, pages = [], initial=True):
		self.PARSER.run(initial=initial, process = self.process_entities, pages = pages)

	def stats(self):

		self.parse_time += int(time()) - self.parse_start_time
		m, s = divmod(self.parse_time , 60)
		h, m = divmod(m, 60)
		formatted = "%d hours, %02d minutes, %02d seconds" % (h, m, s)

		return {
			"exceptions" : self.exceptions,
			"exceptions_count" : len(self.exceptions),
			"duplicate_count" : self.duplicates_count,
			"parsed_items_count" : self.parsed_items_count,
			"time" : {
				"seconds" : self.parse_time,
				"formatted" : formatted
			}
		}

	def formatted_stats(self):
		stats = self.stats()
		exceptions_str = ""

		if stats["exceptions_count"] > 0:
			exceptions_str = "{} exceptions were raised: \n\n".format(stats["exceptions_count"])

			for exception in stats["exceptions"]:
				exceptions_str += (exception + "\n\n")

		return ('Parsed {} items.\n It took: {} \n' + exceptions_str).format(stats["parsed_items_count"], stats["time"]["formatted"])

	def create_record(self, entity, media_type=None, gallery=None, added_at=None, base_url=None):
		if media_type == None:
			media_type = entity["type"]
		if added_at == None:
			added_at = entity.get("added_at")
		if base_url == None:
			base_url = entity.get("base_url")

		caption = entity.get("caption")
		description = entity.get("description")
		surl = entity.get("source_url")

		if media_type == "image":
			return Image(base_url=base_url, caption=caption, description=description, source_url=surl, added_at=added_at, gallery=gallery, provider_id=self.PROVIDER)
		elif media_type == "video":
			return Video(base_url=base_url, caption=caption, description=description, source_url=surl, added_at=added_at, gallery=gallery, provider_id=self.PROVIDER)
		elif media_type == "youtube":
			return YouTube(base_url=base_url, caption=caption, description=description, source_url=surl, added_at=added_at, gallery=gallery, provider_id=self.PROVIDER)
		return None

	def process_entities(self, entities):

		#revrse entites as pages as parsed from top to btm parsed 
		for entity in reversed(entities):

			session = create_scoped_session()
			added_at = parser.parse(entity.get("added_at"))
			media = None

			if entity["type"] == "image":
				media = Image(source_url=entity["data"]["source_url"])
			elif entity["type"] == "video":
				media = Video(source_url=entity["data"]["source_url"])
			elif entity["type"] == "gallery":
				media = Gallery()

				if "images" in "data":

					for index, image_item in enumerate(entity["data"]["images"]):
						image = None

						base_url = entity["base_url"] + "_" + str(index)
						if isinstance(image_item, str):
							image = Image(base_url=base_url, source_url=image_item, added_at=added_at, gallery=media, provider_id=self.PROVIDER)
						elif isinstance(image_item, dict):
							image = self.create_record(image_item, media_type="image", added_at=added_at, gallery=media, base_url=base_url)
						if image != None:
							session.add(image)

				else: #allow galleries of videos, or combidend images and videos
					
					for index, item in enumerate(entity["data"]["items"]):

						base_url = entity["base_url"] + "_" + str(index)
						item = self.create_record(item, base_url=base_url, added_at=added_at, gallery=media)

						if item != None:
							session.add(item)

			if media != None:
				media.base_url = entity["base_url"]
				media.caption = entity.get("caption")
				media.description = entity.get("description")
				media.added_at = added_at
				media.provider_id = self.PROVIDER
				session.add(media)

				try:
					session.commit()
					self.parsed_items_count += 1
					self.DUPLICATE_COUNT = 0
					session.remove()
					print(entity)
				except IntegrityError as e:
					session.remove()
					self.DUPLICATE_COUNT += 1
					self.duplicates_count += 1
					
					if self.DUPLICATE_COUNT > self.DUPLICATE_FINISH_LIMIT:
						self.PARSER.finish()
						break
				except Exception as e:
					session.remove()
					self.exceptions.append({"message" : e, "entity" : entity})
					#send SMS here
					print(e)