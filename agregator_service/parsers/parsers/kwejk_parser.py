from datetime import datetime
from parsers import Parser
from utils import get_soup, is_url, embeded_yt_to_url

class KwejkParser(Parser):

	def __init__(self):
		super(KwejkParser, self).__init__()
		self.REQUIRES_PAGE_LOOKUP = True
		self.PAGE = self.lookup_newest_page()

		self.MAX_WORKERS = 10
		self.last_date = None

		self.pending_youtube_urls = {}

	def lookup_newest_page(self):
		soup = get_soup("http://kwejk.pl")
		return (int(soup.find("div", {"class" : "pagination"}).find("a").get("href").split("/")[-1]) + 1)


	def url(self, page=None):
		if page == None:
			url = "http://kwejk.pl/strona/" + str(self.PAGE)
			if self.PAGE == 0:
				self.finish()
			self.PAGE -= 1
		else:
			url = "http://kwejk.pl/strona/" + str(page)
		return url

	def all_pages(self, start_page = None):
		if start_page == None:
			start_page = self.PAGE
		return ["http://kwejk.pl/strona/" + str(i) for i in range(1, start_page + 1)]

	def parse_list(self, url):
		self.pending_youtube_urls = {}
		soup = get_soup(url)
		urls = []
		for article in soup.find_all("art-ah0"):

			a_tag = article.find("a")
			
			if a_tag != None and is_url(a_tag.get('href')):

				if "youtube.com" in a_tag.get("href"):
					self.pending_youtube_urls[a_tag.get("href")] = a_tag.get("data-track-label")
				urls.append(a_tag.get("href"))

		return urls

	def parse(self, url):

		if url in self.pending_youtube_urls.keys():
			entity = {
				"caption" :  self.pending_youtube_urls[url],
				"data" : {"source_url" : embeded_yt_to_url(url)},
				"base_url" : url,
				"type" : "youtube"
			}
			if self.last_date == None:
				entity["added_at"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
			else:
				entity["added_at"] = self.last_date
			return entity
		else:
			soup = get_soup(url)

			if soup != None:
				media_container = soup.find("div", {"class" : "object"})
				
				if media_container == None:
					return None

				media_type = None
				source_url = None

				caption_tag = soup.find("art-ah0", {"class" : "full"}).find("a") if soup.find("art-ah0", {"class" : "full"}) != None else None

				caption = None
				if caption_tag != None:
					caption = caption_tag.text

				entity = {}

				entity["base_url"] = url
				entity["caption"] = caption

				try:
					self.last_date = soup.find("div", {"class" : "info"}).text.strip(' \t\n\r').split("\n")[1].strip(' \t\n\r ')
					entity["added_at"] = self.last_date
				except:
					if self.last_date == None:
						entity["added_at"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
					else:
						entity["added_at"] = self.last_date

				if caption_tag != None and caption_tag.get("data-track-category") == "Galeria":

					images = []
					for item in soup.find("div", {"class" : "jcarousel"}).find_all("img"):
						images.append(item.get("src").replace("_thumb", ""))

					entity["data"] = {"images" : images}
					media_type = "gallery"

				elif len(media_container.find_all("img")) > 1:

					images = [item.get("src") for item in media_container.find_all("img")]
					entity["data"] = {"images" : images}
					media_type = "gallery"

				elif media_container.find("img") != None:
					source_url = media_container.find("img").get("src")
					media_type = "image"
					entity["data"] = {"source_url" : source_url}
				elif media_container.find("video") != None:
					source_url = media_container.find("video").get("src")
					media_type = "video"
					entity["data"] = {"source_url" : source_url}
				elif media_container.find("iframe") != None:
					media_type = "youtube"
					source_url = media_container.find("iframe").get("src")
					if "youtube.com" in source_url:
						entity["data"] = {"source_url" : embeded_yt_to_url(source_url)}

				entity["type"] = media_type

				if entity.get("data") == None or (entity["data"].get("source_url") == None and entity["data"].get("images") == None):
					return None

				return entity
			return None
