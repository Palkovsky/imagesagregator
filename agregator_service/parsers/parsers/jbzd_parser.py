import re
import time
from parsers import Parser
from utils import get_soup, embeded_yt_to_url
from utils import default_headers
from robobrowser import RoboBrowser
from datetime import datetime

class JbzdParser(Parser):

	def __init__(self):
		super(JbzdParser, self).__init__()
		self.REQUIRES_PAGE_LOOKUP = False
		self.PAGE = 1
		self.MAX_WORKERS = 5

		self.MAX_PAGE = int(self.lookup_max_page())

		self.LOGIN_CREDENTIALS = {"username" : "parser", "password" : "123456789"}
		self.LOGIN_URL = "http://jbzd.pl/logowanie"

		self.COOKIES = self.obtain_login_cookies()

	def url(self, page=None):
		if page == None:
			url = "http://jbzd.pl/strona/" + str(self.PAGE)
			if self.MAX_PAGE == self.PAGE:
				self.finish()
			self.PAGE += 1
		else:
			url = "http://jbzd.pl/strona/" + str(page)
		return url

	def all_pages(self, start_page = None):
		if start_page == None:
			start_page = self.PAGE
		return ["http://jbzd.pl/strona/" + str(i) for i in range(self.MAX_PAGE, 0, -1)]

	def parse_list(self, url):
		time.sleep(1)
		soup = get_soup(url, cookies = self.COOKIES)

		entities = []

		current_date = self.parse_date(soup.find("div", {"class" : "hdate"}))

		for article in soup.find("ul", {"class" : "listing"}).findChildren(recursive = False):

			if article.name == "div":
				current_date = self.parse_date(article)
				continue

			if article.name != "li" or article.get("class") != None:
				continue

			article = article.find("div", {"class" : "content-info"})
			caption_link = article.find("div", {"class" : "title"}).find("a")

			item = {}
			source_url = None
			base_url = caption_link.get("href")
			caption = None
			media_type = None


			if article.find("div", {"class" : "overlay"}) != None:

				item_soup = get_soup(base_url, cookies = self.COOKIES)
				gallery_items = [image.get("src") for image in item_soup.find("div", {"class" : "image"}).find_all("img")]
				item["data"] = {"images" : gallery_items}
				media_type = "gallery"

			elif article.find("img") != None:

				source_url = article.find("img").get("src")
				item["data"] = {"source_url" : source_url}
				media_type = "image"

			elif article.find("video") != None:

				source_url = article.find("video").find("source").get("src")
				item["data"] = {"source_url" : source_url}
				media_type = "video"

			elif article.find("iframe"):

				if article.find("iframe").get("src") != None and "youtube.com" in article.find("iframe").get("src"):
					source_url = embeded_yt_to_url(article.find("iframe").get("src"))
				else:
					yt_soup = get_soup(base_url)
					if yt_soup.find("param", {"name" : "movie"}) != None and "youtube.com" in yt_soup.find("param", {"name" : "movie"}).get("value"):
						source_url = embeded_yt_to_url(yt_soup.find("param", {"name" : "movie"}).get("value"))
				
				item["data"] = {"source_url" : source_url}
				media_type = "youtube"

			if source_url != None or media_type == "gallery":

				caption = caption_link.text

				item["base_url"] = base_url
				item["type"] = media_type
				item["caption"] = caption
				item["added_at"] = current_date
				entities.append(item)

		return entities

	def parse(self, url):
		pass

	def parse_date(self, tag):
		date = datetime.strptime(tag.text.replace(" ", "").replace("\n", "").replace("\r", "/").replace("/", "."), "%d.%m.%Y")
		content = date.strftime("%Y-%m-%d") + " " + datetime.now().strftime('%H:%M:%S')
		return content


	def obtain_login_cookies(self):
		browser = RoboBrowser(history=True, parser = "lxml")
		browser.open(self.LOGIN_URL, headers = default_headers)

		login_form = browser.get_form(action=self.LOGIN_URL)
		login_form["l_email"] = self.LOGIN_CREDENTIALS["username"]
		login_form["l_password"] = self.LOGIN_CREDENTIALS["password"]

		browser.submit_form(login_form)

		return {"ci_session" : browser.session.cookies["ci_session"], "identity" : browser.session.cookies["identity"],
		"remember_code" : browser.session.cookies["remember_code"]}


	def lookup_max_page(self):
		soup = get_soup("http://jbzd.pl")
		js_content = soup.find("body").find("script")

		matched = re.findall(r'[0-9]+', js_content.text)
		return matched[0]