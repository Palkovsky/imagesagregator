import locale, time, re
from parsers import Parser
from utils import get_soup, find_between, format_month_name, to_soup, embeded_yt_to_url, default_headers
from datetime import datetime
from selenium import webdriver
from robobrowser import RoboBrowser

class DemotywatoryParser(Parser):

	def __init__(self):
		super(DemotywatoryParser, self).__init__()

		self.MAX_PAGE = self._lookup_max_page()
		self.MAX_WORKERS = 10
		self.PAGE = 1

		self.last_date = None

		self.LOGIN_CREDENTIALS = {"username" : "parser", "password" : "123456789"}
		self.LOGIN_URL = "http://demotywatory.pl/login"
		self.COOKIES = self.obtain_login_cookies()

	def obtain_login_cookies(self):
		browser = RoboBrowser(history=True, parser = "lxml")
		browser.open(self.LOGIN_URL, headers = default_headers)

		login_form = browser.get_form(action=self.LOGIN_URL)
		login_form["username"] = self.LOGIN_CREDENTIALS["username"]
		login_form["password"] = self.LOGIN_CREDENTIALS["password"]

		browser.submit_form(login_form)

		return {
			"authautologin" : browser.session.cookies["authautologin"],
			"cid" : browser.session.cookies["cid"],
			"demotywatory_pl_session_id" : browser.session.cookies["demotywatory_pl_session_id"],
			"demotywatory_pms_1415293" : browser.session.cookies["demotywatory_pms_1415293"],
			"login_authorization" : browser.session.cookies["login_authorization"]
		}


	def url(self, page=None):
		if page == None:
			url = "http://demotywatory.pl/page/" + str(self.PAGE)
			if self.MAX_PAGE == self.PAGE:
				self.finish()
			self.PAGE += 1
		else:
			url = "http://demotywatory.pl/page/" + str(page)
		return url

	def all_pages(self, start_page = None):
		if start_page == None:
			start_page = self.PAGE
		return ["http://demotywatory.pl/page/" + str(i) for i in range(self.MAX_PAGE, 0, -1)]

	def parse_list(self, url):
		soup = get_soup(url, cookies = self.COOKIES)

		articles = [article for article in soup.find_all("article") if article.find("a", {"class" : "picwrapper"}) != None]
		entities = []

		for article in articles:

			added_at = self._parse_date(article.find("time").text) if article.find("time") != None else self.last_date

			if added_at != None:
				self.last_date = added_at

			if added_at != None:
				entity = {"base_url" : "http://demotywatory.pl" + article.find("a", {"class" : "picwrapper"}).get("href"), "added_at" : added_at}
				entity["caption"] = self._clean_up(article.find("h2").text.strip(' \t\n\r'))

				description_container = article.find("img", {"class" : "demot"})
				entity["description"] = self._clean_up(description_container.get("alt").strip(' \t\n\r')) if description_container != None else None

				if article.find("iframe") != None:
					entity["type"] = "youtube"

					driver = webdriver.PhantomJS()
					driver.get(entity["base_url"])
					yt_soup = to_soup(driver.page_source)

					if yt_soup.find("div", {"class" : "demotivator_inner_video_wrapper youtube"}) != None:
						source_url = yt_soup.find("div", {"class" : "demotivator_inner_video_wrapper youtube"}).find("iframe").get("src")
						entity["data"] = {"source_url" : embeded_yt_to_url(source_url) }

				elif article.find("div", {"class" : " 	 demot_pic 	 image600	 	  gallery 	 	 "}) != None:
					entity["type"] = "gallery"

					gallery_items = []

					gallery_soup = get_soup(entity["base_url"])

					for element in gallery_soup.find_all("div", {"class" : "rsSlideContent"}):				
						caption = None
						caption_container =  element.find("h3")
						if caption_container != None:
							caption = self._clean_up(caption_container.text.strip(' \t\n\r'))

						description = None
						description_container = element.find("p")
						if description_container != None:
							description = self._clean_up(description_container.text.strip(' \t\n\r'))
						if description == "":
							description = None

						source_url = None
						item_type = None

						if element.find("iframe") and element.find("iframe").get("src") != None and "youtube.com" in element.find("iframe").get("src"):
							item_type = "youtube"
							source_url = embeded_yt_to_url(element.find("iframe").get("src"))
						else:
							source_url_container = element.find("img", {"class" : "rsImg"})
							if source_url_container != None:
								item_type = "image"
								source_url = source_url_container.get("src")

						if source_url != None:
							gallery_items.append({"caption" : caption, "description" : description,
								"source_url" : source_url, "type" : item_type})

					entity["data"] = {"items" : gallery_items}
				elif article.find("video") != None:
					entity["type"] = "video"
					entity["data"] = {"source_url" : article.find("video").find("source").get("src") if article.find("video").find("source") != None else None}
				else:
					entity["type"] = "image"
					wrapper = article.find("a", {"class" : "demotivator_inner_image_wrapper "})
					if  wrapper != None and wrapper.find("img") != None and wrapper.find("img").get("src").endswith(".gif"):
						entity["data"] = {"source_url" : wrapper.find("img").get("src")}
					else:
						entity["data"] = {"source_url" : (article.find("img", {"class" : "demot"}).get("src") if article.find("img", {"class" : "demot"}) != None else None)}

				if "data" in entity:
					if ("source_url" in entity["data"] and entity["data"]["source_url"] != None) or "images" in entity["data"] or "items" in entity["data"]:
						entities.append(entity)

		return entities

	def parse(self):
		pass

	def _lookup_max_page(self):
		return int(get_soup("http://demotywatory.pl/").find("div", {"id" : "paginator_3000"}).findNext('script').text.split(",")[1].replace("'", "").replace(" ", "").replace("\t", ""))

	def _parse_date(self, date_string):
		locale.setlocale(locale.LC_ALL, "pl_PL.UTF-8")
		date_string = date_string.replace(" o", "")
		orginal_month_name = date_string.split()[1]
		new_month_name = format_month_name(orginal_month_name)
		date_string = date_string.replace(orginal_month_name, new_month_name)
		return datetime.strptime(date_string, "%d %B %Y %H:%M").strftime("%Y-%m-%d %H:%M")

	def _clean_up(self, string):
		return string.replace("\t", "").replace("\n", "").replace("\r", "")