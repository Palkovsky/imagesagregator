import locale, time
from datetime import datetime
from parsers import Parser
from utils import get_soup, find_between, format_month_name, embeded_yt_to_url, default_headers
from robobrowser import RoboBrowser

class BezuzytecznaParser(Parser):

	def __init__(self):
		super(BezuzytecznaParser, self).__init__()
		
		self.MAX_PAGE = self._lookup_max_page()
		self.MAX_WORKERS = 10
		self.PAGE = 1

		self.LOGIN_CREDENTIALS = {"username" : "parser", "password" : "123456789"}
		self.LOGIN_URL = "http://bezuzyteczna.pl/logowanie"

		self.COOKIES = self.obtain_login_cookies()

	def obtain_login_cookies(self):
		browser = RoboBrowser(history=True, parser = "lxml")
		browser.open(self.LOGIN_URL, headers = default_headers)
		
		login_form = browser.get_form(action="/logowanie")
		login_form["user[username]"] = self.LOGIN_CREDENTIALS["username"]
		login_form["user[password]"] = self.LOGIN_CREDENTIALS["password"]
		browser.submit_form(login_form)

		return {
			"__cfduid" : browser.session.cookies["__cfduid"],
			"auth" : browser.session.cookies["auth"],
			"session" : browser.session.cookies["session"],
			"varnish_c" : browser.session.cookies["varnish_c"]
		}

	def url(self, page=None):
		if page == None:
			url = "http://bezuzyteczna.pl/page/" + str(self.PAGE)
			if self.MAX_PAGE == self.PAGE:
				self.finish()
			self.PAGE += 1
		else:
			url = "http://bezuzyteczna.pl/page/" + str(page)
		return url

	def all_pages(self, start_page = None):
		if start_page == None:
			start_page = self.PAGE
		return ["http://bezuzyteczna.pl/page/" + str(i) for i in range(self.MAX_PAGE, 0, -1)]

	def parse_list(self, url):
		time.sleep(1)
		soup = get_soup(url, cookies = self.COOKIES)

		main_content = soup.find("div", {"id" : "content"})
		articles = main_content.find_all("section", {"class" : "entry_undertext"}, recursive = False) + \
						main_content.find_all("section", {"class" : "entry_gallery"}, recursive = False) + \
						main_content.find_all("section", {"class" : "entry_image"}, recursive = False) + \
						main_content.find_all("section", {"class" : "entry_video"}, recursive = False)

		entities = []

		for article in articles:
			
			entity = {"base_url" : "http://bezuzyteczna.pl" + article.find("a").get("href"), "added_at" : self._parse_date(article.find("div", {"class" : "left"}).find("strong").text)}

			if article.get("class")[0] == "entry_gallery":
				entity["type"] = "gallery"

				entity["base_url"] += "/1"
				
				try:
					gallery_soup = get_soup(entity["base_url"])
					entity["caption"] = gallery_soup.find("section", {"class" : "caption"}).find("h1").text
					entity["data"] = { "images" : [image.get("src").replace("thumbs", "content") for image in gallery_soup.find("div", {"class" : "slider_wrapper"}).find_all("img")] }
				except Exception as e:
					entity = None
			elif article.find("iframe"):
				entity["type"] = "youtube"
				source_url = article.find("iframe").get("src")
				if "youtube.com" in source_url:
					entity["data"] = {"source_url" : embeded_yt_to_url(source_url)}
				else:
					entity = None
			else:
				entity["type"] = "image"
				entity["data"] = { "source_url" : article.find("img").get("src") }

			if entity != None:
				entities.append(entity)

		return entities


	def parse(self, url):
		pass

	def _parse_date(self, date_string):
		locale.setlocale(locale.LC_ALL, "pl_PL.UTF-8")
		date_string = date_string.replace(" o", "")
		orginal_month_name = date_string.split()[1]
		new_month_name = format_month_name(orginal_month_name)
		date_string = date_string.replace(orginal_month_name, new_month_name)
		return datetime.strptime(date_string, "%d %B %Y %H:%M:%S").strftime("%Y-%m-%d %H:%M:%S")

	def _lookup_max_page(self):
		soup = get_soup("http://bezuzyteczna.pl")
		javascript_content = soup.find("div", {"id" : "paginator"}).findNext('script').text

		javascript_content = javascript_content.replace(",", "|", 2)

		try:
			page = int(find_between(javascript_content, "|", "|"))
		except:
			page = 100000

		return page