import concurrent.futures, time
from abc import ABC, abstractmethod

from utils import get_soup

#find a way to test parsers

class Parser(ABC):

	def __init__(self):
		self.ACTIVE = True
		self.PAGE = 0

		self.MAX_WORKERS = 25

		self.REQUIRES_PAGE_LOOKUP = False

		self.current_url = None

	'''
		called every time one list and child entites are parsed
		allows you to edit an url
	'''
	@abstractmethod
	def url(self, page=None):
		pass

	'''
		used when initializing. returns list of all pages of website
	'''
	@abstractmethod
	def all_pages(self, start_page = None):
		pass

	'''
		parse_list returns list of urls to entites(if REQUIRES_PAGE_LOOKUP = True) or
		list of entites
	'''
	@abstractmethod
	def parse_list(self, url):
		pass	

	'''
		parse pares media resource and returns an media object
	'''
	@abstractmethod
	def parse(self, url):
		pass

	'''
		process shall be callable accepting list arg
	'''
	def run(self, initial = False, max_workers = None, process = lambda params:(None), start_page = None, pages=[]):
		if max_workers == None:
			max_workers = self.MAX_WORKERS
		if start_page == None:
			start_page = self.PAGE

		urls = [self.url(page=page) for page in pages]

		if self.REQUIRES_PAGE_LOOKUP:
			if initial:
				urls = self.all_pages(start_page = start_page) if len(urls) == 0 else urls
				with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
					for entities in executor.map(self.__parse_page_with_lookup, urls):
						process(entities)
			else:
				while self.ACTIVE:
					urls = self.parse_list(self.url()) if len(urls) == 0 else urls
									
					with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
						for entity in executor.map(self.parse, urls):
							process([entity])
		else:
			if initial:
				urls = self.all_pages(start_page = start_page) if len(urls) == 0 else urls

				with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
					for entities in executor.map(self.__parse_page, urls):
						process(entities)
			else:
				while self.ACTIVE:
					if len(urls) == 0:
						process(self.__parse_page(self.url()))
					else:
						for url in urls:
							process(self.__parse_page(url))


	def finish(self):
		self.ACTIVE = False

	def sanitize(self, entities):
		VIDEO_FORMATS = (".webm", ".mp4", ".WEBM", ".MP4")
		IMAGES_FORMATS = (".jpg", ".jpeg", ".png", ".gif", ".bnp", ".JPG", ".JPEG", ".PNG", ".GIF", ".BNP")

		sanitized_entities = []
		for entity in entities:

			source_url = entity["data"].get("source_url")
			if source_url != None and '?' in source_url:
				source_url = source_url[:source_url.find('?')]

			if entity["type"] == "image":
				if source_url.endswith(IMAGES_FORMATS):
					sanitized_entities.append(entity)
			elif entity["type"] == "video":
				if source_url.endswith(VIDEO_FORMATS):
					sanitized_entities.append(entity)
			else:
				sanitized_entities.append(entity)

		return sanitized_entities

	def __parse_page_with_lookup(self, url):
		urls = self.parse_list(url)
		print(url)
		entities = []
		for i in range(len(urls)):
			entity = self.parse(urls[i])
			if entity != None:
				entities.append(entity)
		return self.sanitize(entities)

	#like above, but no lookup required
	def __parse_page(self, url):
		print(url)
		entities = []
		for entity in self.parse_list(url):
			if entity != None:
				entities.append(entity)
		return self.sanitize(entities)

	#does one of two above
	def parsed_page(self, url):
		if self.REQUIRES_PAGE_LOOKUP:
			return self.__parse_page_with_lookup(url)
		else:
			return self.__parse_page(url)
