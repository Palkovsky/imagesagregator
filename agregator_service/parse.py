import argparse, os
from datetime import datetime
from parsers import * 
from models import Provider, Log
from db import create_session, redis_server

def format_message(error, stats, exception = "", initial = False):
	if error:
		return '''While parsing, an FATAL error occured: \n\n ''' + str(exception) + '''
				\n\n\n Initial: ''' +str(initial) + '''\n Statistics: ''' + stats
	else:
		return '''Parsing successfull. \n
			Initial: ''' +str(initial) + '''\n\n\n Statistics:\n''' + stats


#parsers is a list of key values from above
def execute(initial, providers = None):

	session = create_session()

	PARSERS = {
		"demotywatory" : demotywatory_parser.DemotywatoryParser,
		"jbzd" : jbzd_parser.JbzdParser,
		"kwejk" : kwejk_parser.KwejkParser,
		"bezużyteczna" : bezuzyteczna_parser.BezuzytecznaParser
	}

	if providers == None:
		providers = session.query(Provider).filter_by(initially_parsed = not initial)

	for provider in providers:
		Parser = PARSERS.get(provider.name.lower())

		if Parser != None:

			controller = ParseController(Parser(), provider = provider.id)

			try:
				if provider.initially_parsed == False or provider.initially_parsed == None:
					redis_server.set("initial_parsing", "True")
					redis_server.set("initial_parsing_provider", provider.id)
					controller.start_initial_parsing()
				else:
					redis_server.set("parsing", "True")
					redis_server.set("parsing_provider", provider.id)
					controller.start_parsing()

					
				provider.initially_parsed = True
				provider.last_parse = datetime.now()
				session.add(provider)
				session.add(Log(error = False, provider_id = provider.id, message = format_message(False, controller.formatted_stats(), initial = initial)))
				session.commit()

				if initial:
					redis_server.set("initial_parsing", "False")
					redis_server.set("initial_parsing_provider", None)
				else:
					redis_server.set("parsing", "False")
					redis_server.set("parsing_provider", None)

				redis_server.set("last_successful_parse", datetime.now().strftime('%d-%m-%Y %H:%M'))
				print("Parsing finished.")
			except Exception as e:
				print(e)
				session.add(Log(error = True, provider_id = provider.id, message = format_message(True, controller.formatted_stats(), exception = e, initial = initial)))
				session.commit()

				if initial:
					redis_server.set("initial_parsing", "False")
					redis_server.set("initial_parsing_provider", None)
				else:
					redis_server.set("parsing", "False")
					redis_server.set("parsing_provider", None)

			redis_server.set("last_parse", datetime.now().strftime('%d-%m-%Y %H:%M'))

#execute(True)