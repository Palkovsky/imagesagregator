<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
	<title>Panel Administracjny</title>
</head>
<body>

	<a href="/"><h2>HOME</h2></a>
	<a href="/parser"><h2>PARSER</h2></a>

    @if (Session::has('flash_notification.message'))
		{{ Session::get('flash_notification.message') }}
	@endif

	<div class="container">
		@yield('content')
	</div>
</body>
</html>