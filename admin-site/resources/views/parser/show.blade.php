@extends('layouts.default')

@section('content')


	<div class="parser_info"> 
		@if($stats->parsing == "true")
			<p>Właśnie odbywa się parsowanie providera o ID:  <a href="/provider/{{ $stats->parsing_provider }}"><strong>{{$stats->parsing_provider}}</strong></a></p>
		@else
			<p>W tej chwili na serwerze nie odbywa się żadne parsowanie.</p>
		@endif

		@if($stats->initial_parsing == "true")
			<p>Właśnie odbywa się parsowanie wstępne providera o ID:  <a href="/provider/{{ $stats->initial_parsing_provider }}"><strong>{{$stats->initial_parsing_provider}}</strong></a></p>
		@else
			<p>W tej chwili nie odbywa się żadne parsowanie wstępne na serwerze.</p>
		@endif
	</div>


	<div class="parser_config">
		<ul>
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
		</ul>

		{!! Form::open(array('action' => array('ParserController@config'), 'method' => 'post')) !!}

			{{ Form::label('parse_interval', 'Odstęp parsowania:') }}<br/>
		    {{ Form::number('parse_interval', $config->parse_interval)}}<br/>

		    {{ Form::label('initial_parse_interval', 'Odstęp wstępnego parsowania:') }}<br/>
		    {{ Form::number('initial_parse_interval', $config->initial_parse_interval)}}<br/>

			{!! Form::submit('Zapisz') !!}
		{!! Form::close() !!}
	</div>
@endsection