@extends('layouts.default')

@section('content')
	<h2>Edycja dostawcy</h2>

	<ul>
	@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
	</ul>


	{!! Form::open(array('action' => array('ProvidersController@update', $provider->id), 'method' => 'patch')) !!}

		{{ Form::label('url', 'URL:') }}<br/>
	    {{ Form::text('url', $provider->url)}}<br/>

	    {{ Form::label('name', 'Nazwa:') }}<br/>
	    {{ Form::text('name', $provider->name)}}<br/>

	    {{ Form::label('thumbnail_url', 'URL miniaturki:') }}<br/>
	    {{ Form::text('thumbnail_url', $provider->thumbnail_url)}}<br/>

	    {{ Form::label('description', 'Opis:') }}<br/>
	    {{ Form::textarea('description', $provider->description)}}<br/>

		{!! Form::submit('Zapisz') !!}
	{!! Form::close() !!}

	<br/>
	<a href="/providers">Wróć</a>
@endsection