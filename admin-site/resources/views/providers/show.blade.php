@extends('layouts.default')

@section('content')
	@if($provider != NULL)
		<h1><a href="{{ $provider->url }}">{{ $provider->name }}</a> | ({{ $provider->item_count }})</h1>
		@if ($provider->initially_parsed == "true")
			<p>Wstępne parsowanie zostało zrobione.</p>
			<h4>Data: {{ $provider->last_parse }}</h4>
		@else
			<p>Dostawca nie został jeszcze wstępnie sparsowany.</p>
		@endif

		@if ($stats->initial_parsing_provider == $provider->id)
			<h3>WŁAŚNIE ODBYWA SIĘ WSTĘPNE PARSOWANIE! (Usunięcie niemożliwe)</h3>
		@elseif ($stats->parsing_provider == $provider->id)
			<h3>WŁAŚNIE ODBYWA SIĘ PARSOWANIE! (Usunięcie niemożliwe)</h3> 
		@else
			{!! Form::open(array('action' => array('ProvidersController@destroy', $provider->id), 'method' => 'delete')) !!}
				{{ Form::submit('Usuń dostawcę') }}
			{!! Form::close() !!}
		@endif

		<div class="provider_logo"> 
			@if ($provider->thumbnail_url != NULL)
				<img src="{{$provider->thumbnail_url}}">
			@endif
		</div>

		<div class="provider_info">
			<strong>Opis: </strong> {{ $provider->description }}<br/>
			<a href="/provider/{{$provider->id}}/edit">Edycja</a>
		</div>

		<div id="log_pane">
			<h2>Ostatnie <a href="{{$provider->id}}/logs">Logi</a></h2>
			@if(count($logs) > 0)
				@foreach($logs as $log)
					<div class="log">
						<h5>{{ $log->created_at }}</h5>
						<textarea rows="15" cols="70" readonly>
							{{ $log->message }}
						</textarea>
					</div>
				@endforeach
			@else
				<p>Brak logów</p>
			@endif
		</div>

		<div id="media_pane">
			<h2>Ostatnio dodane <a href="{{$provider->id}}/medias">elementy</a></h2>
			@if(count($medias) > 0)
				@foreach($medias as $media)
					<div class="media">
						<a href="{{ $media->base_url }}">{{ $media->base_url }}</a> | {{ $media->media_type }}
					</div>
				@endforeach
			@else
				<p>Brak</p>
			@endif
		</div>
	@else
		<h1>Niepoprawny dostawca.</h1>
	@endif
	<br/>
	<a href="/providers">Wróć</a>
@endsection