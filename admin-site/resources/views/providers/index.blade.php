@extends('layouts.default')

@section('content')
	@if(count($providers) > 0)
		<ul class="providers">
			@foreach($providers as $provider)
				<li><a href="provider/{{$provider->id}}">{{$provider->name}} | {{$provider->item_count}}</a></li>
			@endforeach
		</ul>
	@else
		<h3>Brak dostawców.</h3>
	@endif

	<div class="parser_info"> 
		@if($stats->parsing == "true")
			<p>Właśnie odbywa się parsowanie providera o ID:  <a href="/provider/{{ $stats->parsing_provider }}"><strong>{{$stats->parsing_provider}}</strong></a></p>
		@else
			<p>W tej chwili na serwerze nie odbywa się żadne parsowanie.</p>
		@endif

		@if($stats->initial_parsing == "true")
			<p>Właśnie odbywa się parsowanie wstępne providera o ID:  <a href="/provider/{{ $stats->initial_parsing_provider }}"><strong>{{$stats->initial_parsing_provider}}</strong></a></p>
		@else
			<p>W tej chwili nie odbywa się żadne parsowanie wstępne na serwerze.</p>
		@endif
	</div>

	<a href="/providers/new">Stwórz nowego dostawcę</a>
@endsection