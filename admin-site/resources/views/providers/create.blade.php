@extends('layouts.default')

@section('content')
	<h2>Nowy dostawca</h2>

	<ul>
	@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
	</ul>


	{!! Form::open(array('url' => '/providers', 'method' => 'post')) !!}

		{{ Form::label('url', 'URL:') }}<br/>
	    {{ Form::text('url')}}<br/>

	    {{ Form::label('name', 'Nazwa:') }}<br/>
	    {{ Form::text('name')}}<br/>

	    {{ Form::label('thumbnail_url', 'URL miniaturki:') }}<br/>
	    {{ Form::text('thumbnail_url')}}<br/>

	    {{ Form::label('description', 'Opis:') }}<br/>
	    {{ Form::textarea('description')}}<br/>

		{!! Form::submit('Dodaj') !!}
	{!! Form::close() !!}

	<br/>
	<a href="/providers">Wróć</a>
@endsection