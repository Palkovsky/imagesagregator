<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Laracasts\Flash\Flash;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class ParserController extends Controller
{
	public function show(){
		$client = new Client(['base_uri' => env("BASE_URI")]);
		$stats = json_decode($client->request('GET', 'stats')->getBody())->data;
		$config = json_decode($client->request('GET', 'config')->getBody())->data;
		return view('parser/show', ["stats" => $stats, "config" => $config]);
	}

	public function config(){
		$rules = array(
			'initial_parse_interval' => 'required|integer',
			'parse_interval' => 'required|integer'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
            return Redirect::to('/parser')
                ->withErrors($validator)
                ->withInput(Input::all());
		}else{
			$client = new Client(['base_uri' => env("BASE_URI")]);

			try{
				$response = $client->request('POST', 'config', ['json' => Input::all()]);
				$data = json_decode($response->getBody())->data;
 				Flash::message('OK');
 				return Redirect::to('/parser');
			}catch(ClientException $e){
	            return Redirect::to('/parser')
	                ->withErrors(["Server couldn't accept the request."])
	                ->withInput(Input::all());
			}
		}
	}
}