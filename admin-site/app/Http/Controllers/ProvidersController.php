<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Laracasts\Flash\Flash;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class ProvidersController extends Controller
{

	public function index(){
		$client = new Client(['base_uri' => env("BASE_URI")]);
	    $data = json_decode($client->request('GET', 'providers')->getBody());
	    $stats = json_decode($client->request('GET', 'stats')->getBody());
	    return view('providers/index', ["providers" => $data->data, "stats" => $stats->data]);
	}

	public function show($provider_id){
		$client = new Client(['base_uri' => env("BASE_URI")]);

		try{
			$response = $client->request('GET', 'providers/'.$provider_id);
			$provider = json_decode($response->getBody())->data;
			$logs = json_decode($client->request('GET', 'logs?per=8&providers='.$provider_id)->getBody())->data;
			$medias = json_decode($client->request('GET', 'medias?providers='.$provider_id)->getBody())->data;
			$stats = json_decode($client->request('GET', 'stats')->getBody())->data;
		}catch(ClientException $e){
			$provider = NULL;
			$stats = NULL;
			$logs = [];
			$medias = [];
		}

		return view('providers/show', ["provider" => $provider, "logs" => $logs, "medias" => $medias, "stats" => $stats]);
	}

	public function create(){
		return view('providers/create');
	}

	public function store(){
		$rules = array(
			'url' => 'required|url',
			'name' => 'required',
			'description' => '',
			'thumbnail_url' => 'url'
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
            return Redirect::to('/providers/new')
                ->withErrors($validator)
                ->withInput(Input::all());
		}else{
			$client = new Client(['base_uri' => env("BASE_URI")]);

			try{
				$response = $client->request('POST', 'providers', ['json' => Input::all()]);
				$data = json_decode($response->getBody())->data;
 				Flash::message('Dodano prawidłowo!');
 				return Redirect::to('/providers');
			}catch(ClientException $e){
	            return Redirect::to('/providers/new')
	                ->withErrors(["Server couldn't accept the request. Probably provider with given name or URL already exists."])
	                ->withInput(Input::all());
			}
		}
	}

	public function edit($provider_id){
		$client = new Client(['base_uri' => env("BASE_URI")]);
		try{
			$provider = json_decode($client->request('GET', 'providers/'.$provider_id)->getBody())->data;
			return view("providers/edit", ["provider" => $provider]);
		}catch(ClientException $e){
			Flash::message("Nie znaleziono dostawcy.");
			return Redirect::to('/providers');
		}
	}

	public function update($provider_id){
		$rules = array(
			'url' => 'required|url',
			'name' => 'required',
			'description' => '',
			'thumbnail_url' => 'url'
		);


		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
            return Redirect::to('/providers/'.$provider_id.'/edit')
                ->withErrors($validator)
                ->withInput(Input::all());
		}else{
			$client = new Client(['base_uri' => env("BASE_URI")]);
			try{
				$response = $client->request('PUT', '/providers/'.$provider_id, ['json' => Input::all()]);
				$data = json_decode($response->getBody())->data;
 				Flash::message('Aktualizowanie pomyślne.');
 				return Redirect::to('/provider/'.$provider_id);
			}catch(ClientException $e){
	            return Redirect::to('/providers/'.$provider_id.'/edit')
	                ->withErrors(["Server couldn't accept the request. Probably provider with given name or URL already exists."])
	                ->withInput(Input::all());	
			}
		}
	}

	public function destroy($provider_id){
		$client = new Client(['base_uri' => env("BASE_URI")]);

		try{
			$response = $client->request('DELETE', 'providers/'.$provider_id);
			Flash::message('Usunięto');
		}catch(ClientException $e){

		}
    	return Redirect::to('/providers');		
	}
}
