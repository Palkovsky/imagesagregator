<?php

#show lists of providers
Route::get('/', ['uses' => 'ProvidersController@index']);
Route::get('/providers', ['uses' => 'ProvidersController@index']);

#Display form for new provider
Route::get('/providers/new', ['uses' => 'ProvidersController@create']);

#Try to create new provider
Route::post('/providers', ['uses' => "ProvidersController@store"]);

#shows sepcific info about provider(logs, entries etc.)
Route::get('/provider/{id}', ['uses' => 'ProvidersController@show']);

#displays edit form
Route::get('/provider/{id}/edit', ['uses' => 'ProvidersController@edit']);
Route::patch('/provider/{id}', ['uses' => 'ProvidersController@update']);

#delete provider
Route::delete('/provider/{id}', ['uses' => 'ProvidersController@destroy']);

Route::get('/parser', ['uses' => 'ParserController@show']);
Route::post('/parser/config', ['uses' => 'ParserController@config']);