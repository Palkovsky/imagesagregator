angular.module('agregatorApp')
.controller('mediaCtrl', ['$http', "$sce", function($http, $sce){
	
	var self = this;

	var BASE_URL = "http://localhost:8000";
	var PROVIDER_COLUMNS = 4;
	var PER_PAGE = 10;

	var page = 1;

	var fetched_providers = false;
	var fetched_all = false;

	self.new_entries = 0;
	self.fetching_medias = false;
	self.providers = [];
	self.medias = [];

	var linear_providers = [];
	var selected_providers = [];

	self.fetch_media = function(){
		if(fetched_providers && !self.fetching_medias && !fetched_all){

			self.fetching_medias = true;

			$http.get(buildMediasUrl()).then(function(response){
				var fetched_medias = response.data["data"];

				fetched_all = (fetched_medias.length == 0);

				for(i=0; i<fetched_medias.length; i++){
					if(fetched_medias[i]["media_type"] == "video"){

						mime_type = "video/mp4";
						if(fetched_medias[i]["data"]["source_url"].endsWith(".webm")){
							mime_type = "video/webm";
						}

						fetched_medias[i]["data"] = {src: $sce.trustAsResourceUrl(fetched_medias[i]["data"]["source_url"]), type: mime_type};
					}
				}

				//make sure we won't put duplicates into dataset
				var i = fetched_medias.length;
				while(i--){
					if(getDictByKeyValue(self.medias, "id", fetched_medias[i]["id"]) != undefined){
						fetched_medias.splice(i, 1);
						self.new_entries += 1;
					}
				}

				console.log(self.new_entries);

				self.medias = self.medias.concat(fetched_medias);
				page += 1;
				self.fetching_medias = false;
			});
		}
	};

	self.switch_provider = function(provider){
		id = provider.id
		index = selected_providers.indexOf(id);
		if(index == -1){
			selected_providers.push(id);
		}else{
			selected_providers.splice(index, 1);
		}

		self.restart();
	};

	self.restart = function(){
		fetched_all = false;
		fetched_medias = false;
		page = 1;
		self.new_entries = 0;
		self.medias = [];
		self.fetch_media();		
	}

	self.isEmpty = function(){
		return (self.medias.length == 0 && !self.fetched_medias && !self.fetching_medias)
	}

	self.update_providers = function(){
		$http.get(BASE_URL + '/providers/').then(function(response){
			linear_providers = response.data["data"];

			self.providers = [];
			for(i=0, j=linear_providers.length; i<j; i+=PROVIDER_COLUMNS){
				self.providers.push(linear_providers.slice(i, i + PROVIDER_COLUMNS));
			}
		});		
	}

	//ng-class meth
	self.is_selected = function(provider){
		return {
			"selected" : (selected_providers.indexOf(provider.id) != -1),
			"selectable" : true
		};
	};


	//utils meth
	var buildMediasUrl = function(){
		url = BASE_URL + "/medias";
		url += "?page=" + page + "&per=" + PER_PAGE + "&providers=";
		for(i=0; i < selected_providers.length; i++){
			if(i == selected_providers.length - 1){
				url += (selected_providers[i]);
			}else{
				url += (selected_providers[i] + ",");
			}
		}
		return url;
	}

	var getDictByKeyValue = function(dict_array, key, value){
		for(i = 0; i < dict_array.length; i++){
			if(dict_array[i][key] === value){
				return dict_array[i];
			}
		}
		return undefined;
	}


	//logic
	$http.get(BASE_URL + '/providers/').then(function(response){
		linear_providers = response.data["data"];
		for(i = 0; i<linear_providers.length; i++){
			selected_providers.push(linear_providers[i].id);
		}
		for(i=0, j=linear_providers.length; i<j; i+=PROVIDER_COLUMNS){
			self.providers.push(linear_providers.slice(i, i + PROVIDER_COLUMNS));
		}

		fetched_providers = true;

		self.fetch_media();

		setInterval(self.update_providers, 1500);
	});
}]);