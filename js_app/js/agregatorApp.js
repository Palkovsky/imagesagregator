'use strict';
angular.module('agregatorApp', [
		"ngSanitize",
		"infinite-scroll",
		"angular.backtop",
		"angular-inview"
	]);